# shell-a11y

Ce dépot vise à donner des conseils pour faciliter l'usage de la ligne de commande pour les personnes aveugles ou malvoyantes.

Il contient également une liste d'alias bash et de fonctions qui visent à réduire à l'essentiel les sorties des commandes les plus communes.

## Installation

Vous pouvez lancer ces commandes pour installer l'ensemble de ces alias.
```
apt install git
git clone https://codeberg.org/ARN/shell-a11y.git ~/.shell-a11y
echo ". ~/.shell-a11y/bash_aliases" >> ~/.bash_aliases
```

Il est aussi possible de les activer sélectivement. Par exemple, pour activer uniquement l'alias simplifiant `dig`:
```
echo ". ~/.shell-a11y/bash_aliases.d/dig.sh" >> ~/.bash_aliases
```
## TODO

 - [x] Création d'un alias pour copier la sortie dans le presse papier
 - [x] Alias pour simplifier l'usage de ping (selon le cas d'usage)
 - [ ] Stabiliser et améliorer le concept de l'alias notif pour conserver le status
   - [ ] Beep distinct selon le succés de la commande
   - [ ] Evaluer la nuisance du beep visuel dans la sortie
   - [ ] Evaluer la nuisance du beep visuel en mode blanc pour la lecture en cas de commande coincée
 - [ ] Alias pour rendre la commande apt moins verbeuse
 - [x] Alias pour filtrer la sortie d'erreur de wget
 - [ ] Alias pour explorer/chercher des infos sur les processsus
 - [ ] Alias pour netstats
 - [ ] Alias pour systemctl (listage des services)
 - [ ] Réflexion sur les commandes dmesg, namei, rsync
 - [ ] Traduction anglaise
