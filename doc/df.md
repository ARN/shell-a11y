# df


## SYNOPSIS

`df [FILTRE] [OPTIONS] [FICHIER...]`

## DESCRIPTION
La commande `df` permet de récupérer des informations sur l'espace disponible sur les partitions. 

Cette surcouche permet de filtrer la sortie originale de `df` pour n'afficher que les informations recherchées les plus courrantes. Elle permet aussi d'éviter l'affichage de tableau multi-colonnes.

Par défaut, l'alias exclut les partitions de type squashfs ou tmpfs dont les informations sont souvent peu utiles. Il active également l'option `-h` pour éviter la lecture de taille en octet (nombre trop grand). 

Rappel: vous pouvez continuer à utiliser la commande originale en tapant: `\df`

## FILTRE

 * `df free`: Obtenir l'espace disponible des partitions
 * `df used`: Obtenir l'espace utilisé des partitions
 * `df size`: Obtenir l'espace totale des partitions
 * `df source`: Obtenir la partition associée à un chemin

## OPTIONS
Identique aux options de la commande originale

## CONSEIL D'USAGE

Si on souhaite des informations sur une partition ou sur un chemin précis, il est conseillé de le spécifier après la commande. De cette façon, la sortie sera uniquement constituées de la valeur recherchée.

## AUTEURICES
ljf et irina
