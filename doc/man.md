# man

## SYNOPSIS

```
man [OPTIONS]
```

## DESCRIPTION

La commande man permet d'obtenir le manuel d'une commande.

Cet alias permet de charger la page dans le navigateur web Firefox afin de faciliter le parcours du manuel grâce au lien et à la navigation par titre.

Rappel: vous pouvez continuer à utiliser la commande originale en tapant: `\man`

## AUTEURICE
ljf
