# mount

## SYNOPSIS

`lsmount`

## DESCRIPTION

La commande mount permet notamment de lister tous les points de montage.

Cet alias propose en alternative de ne lister que les points de montages les plus pertinents.

## AUTEURICE
ljf
