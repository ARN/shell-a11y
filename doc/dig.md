# dig

## SYNOPSIS

`dig ...`

## DESCRIPTION

La commande `dig` permet d'interroger un serveur DNS.

Cet alias permet de rendre la commande dig moins verbeuse en activant l'option `+short` par défaut.

Rappel: vous pouvez continuer à utiliser la commande originale en tapant: `\dig`

## EXEMPLE

```
$ dig A wikipedia.org
185.15.58.224
```

## AUTEURICE
ljf
