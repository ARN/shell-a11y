# copy

## SYNOPSIS

`COMMAND | copy [-q]`

## DESCRIPTION

Cet alias permet de copier la sortie d'une autre commande dans le presse papier.

## OPTIONS

`-q` Empèche la sortie standard d'être affichée dans la console.

## EXEMPLE

`curl https://ip.yunohost.org | copy`

## AUTEURICE
ljf

