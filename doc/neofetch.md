# neofetch

## SYNOPSIS

`neofetch ...`

## DESCRIPTION

La commande neofetch affiche un logo qui est lu par le lecteur d'écran ou la plage braille, il est proposé ici de le désactiver.

Rappel: vous pouvez continuer à utiliser la commande originale en tapant: `\neofetch`

## AUTEURICE
ljf
