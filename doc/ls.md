# ls

## SYNOPSIS

```
ls [OPTIONS]
```

## DESCRIPTION

La commande ls permet de lister le contenu d'un dossier.

Cet alias permet:
 * d'activer l'indicateur / pour savoir facilement si il s'agit d'un dossier ou d'un fichier
 * de regrouper les dossiers au début pour une meilleure compréhension
 * d'activer les couleurs
 * d'afficher une entrée par ligne pour simplifier la navigation au clavier dans la sortie

Rappel: vous pouvez continuer à utiliser la commande originale en tapant: `\ls`

## EXEMPLE

```
$ ls -p1 --group-directories-first --color=auto
Bureau/
Documents/
Images/
Musique/
Public/
Téléchargements/
Vidéos/
Firefox_wallpaper.png
```

## AUTEURICE
ljf
