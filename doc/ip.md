# ip

## SYNOPSIS

```
ip [LINK] [OPTIONS]  : Obtenir et manipuler les IP pour chaque interface ou pour une interface spécifique
ip4 [LINK] [OPTIONS] : Idem mais en se restreignant aux IPv4
ip6 [LINK] [OPTIONS] : Idem mais en se restreignant aux IPv6
public_ip4     : Obtenir une ipv4 publique utilisée pour accéder à internet
public_ip4     : Obtenir une ipv6 publique utilisée pour accéder à internet
link           : Obtenir la liste des interfaces
mac [eno1]     : Obtenir les adresses mac
route4         : Obtenir et manipuler les routes en IPv4
route6         : Obtenir et manipuler les routes en IPv6
```


## DESCRIPTION
La commande ip permet de gérer de nombreux aspects du réseau (adresse, liens, mac, routes, etc.). Toutefois, la plupart des sorties sont très verbeuses.

Ce jeu d'alias permet d'obtenir de façon unitaire (ou presque) les informations les plus courantes.

Rappel: vous pouvez continuer à utiliser la commande originale en tapant: `\ip`.

## OPTIONS
Identique aux options de la commande originale

## EXEMPLE
Obtenir la liste des interfaces
```
$ link
eno1
lo
```

Obtenir les IP pour chaque interface
```
$ ip
eno1             UP             192.168.1.2/24 2a02:842::1/128 fe80::2992::1/64 
lo               UNKNOWN        127.0.0.1/8 ::1/128
```

Obtenir les IPv4 pour chaque interface
```
$ ip4
eno1             UP             192.168.1.2/24 
lo               UNKNOWN        127.0.0.1/8
```

Obtenir les IPv4 pour une interface spécifique
```
$ ip4 eno1
 192.168.1.2/24 
```

Obtenir les IPv4 pour une interface spécifique
```
$ ip4 eno1
 192.168.1.2/24 
```

Obtenir les IPv4 pour une interface spécifique
```
$ public_ip4
89.234.141.xx
```

Utiliser la commande `ip` pour observer ou manipuler la config réseau de la même façon qu'avec la commande originale (les options -c et -br en plus).
```
$ ip addr add 10.240.0.1 dev eno1
```

Obtenir l'adresse mac lié à une interface
```
$ mac
aa:bb:cc:dd:ee:ff
```

Obtenir les routes en IPv4
```
$ route4
default via 192.168.1.1 dev eno1 proto dhcp src 192.168.1.155 metric 100 
192.168.1.0/24 dev eno1 proto kernel scope link src 192.168.1.155 metric 100
```


## AUTEURICE
ljf
